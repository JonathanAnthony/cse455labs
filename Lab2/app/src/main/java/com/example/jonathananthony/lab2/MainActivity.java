package com.example.jonathananthony.lab2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button button;

    EditText in;
    EditText out;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        button = (Button)findViewById(R.id.button);

        in = (EditText)findViewById(R.id.editText);
        out = (EditText)findViewById(R.id.editText2);

        button.setOnClickListener(new View.OnClickListener(){
            @Override
           public void onClick(View v){
                String inputValue = in.getText().toString();
                out.setText(inputValue);
            }


        });
    }
}
